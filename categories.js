const express = require('express');
const router = express.Router();
const file = require('./invent');


router.get('/', (req, res) => {
  let category = file.getCategory();
  res.send(category);
});

router.get('/:id', (req, res) => {
  const category = file.getCat(req.params.id);
  return res.send(category);
});


module.exports = router;