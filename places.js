const express = require('express');
const router = express.Router();
const file = require('./invent');


router.get('/', (req, res) => {
  let place = file.getPlaces();
  res.send(place);
});

router.get('/:id', (req, res) => {
  const places = file.getPlace(req.params.id);
  return res.send(places);
});


module.exports = router;