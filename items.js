const express = require('express');
const router = express.Router();
const file = require('./invent');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('./config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
})

const  upload = multer({storage});

router.get('/', (req, res) => {
  let imageBoard = file.getItems();
  res.send(imageBoard);
});

router.get('/:id', (req, res) => {
  const items = file.getItem(req.params.id);
  return res.send(items);
});

router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    const items = {
      title: req.body.title,
      description: req.body.description,
    };
  const place = {
    'place': req.body.place,
    'placeDescription': req.body.placeDescription,
  };
  const category = {
    'category': req.body.category,
    'catDescription': req.body.catDescription,
  };
    if (req.file){
      items.image = req.file.filename;
    }
    await file.addItem(items, place, category);

    return res.send({message: 'created new message', id: items.id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;