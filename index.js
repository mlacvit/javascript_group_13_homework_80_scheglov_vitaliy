const express = require('express');
const app = express();
const cors = require('cors');
const files = require('./invent');
const items = require('./items');
const places = require('./places');
const categories = require('./categories');
const port = 8000;

app.use(cors());
app.use(express.static('public'));
app.use(express.json());
app.use('/items', items);
app.use('/places', places);
app.use('/categories', categories);

const run = async () => {
  await files.init();
}

app.listen(port, () => {
  console.log('We are live on ' + port);
});

run().catch(e => console.error(e));