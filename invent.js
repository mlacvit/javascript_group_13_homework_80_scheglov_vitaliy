const {nanoid} = require("nanoid");

const fs = require('fs').promises;
const filename = './text.json';
let data = {
  'items': [],
  'places': [],
  'categories': [],
};

module.exports = {
  async init() {
    try {
      const fileContents = await fs.readFile(filename);
      data = JSON.parse(fileContents.toString());
    } catch (e) {
      data = [];
    }
  },

  getItems() {
    return data;
  },
  getPlaces() {
    return data.places;
  },
  getCategory() {
    return data.categories;
  },

  getItem(id){
    return data.items.find(p => p.id === id);
  },
  getPlace(id){
    return data.places.find(p => p.id === id);
  },
  getCat(id){
    return data.categories.find(p => p.id === id);
  },

  addItem(item, place, cat) {
    place.id = nanoid();
    cat.id = nanoid();
    item.id = nanoid();
    item.place = place.id;
    item.category = cat.id;
    data.push(item);
    data.push(place);
    data.push(cat);
    return this.save();
  },

  save() {
    return fs.writeFile(filename, JSON.stringify(data));
  }
};